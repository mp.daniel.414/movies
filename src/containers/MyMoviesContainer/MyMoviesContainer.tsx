import MovieList from "../../components/MovieList"
import useDispatch from "../../hooks/useDispatch";
import useSelector from "../../hooks/useSelector";
import { removeMovie } from "../../redux/action";
import { MovieRootState } from "../../redux/store";

const MyMoviesContainer = () => {
  const dispatch = useDispatch();
  const { allMovies, loading, favorityMovies } = useSelector<MovieRootState>(state => ({
    favorityMovies: state.favorityMovies,
    allMovies: state.allMovies,
    loading: state.loading,
  }));
  return !loading ? (
    <MovieList
      title="My Movies"
      movies={allMovies.filter(movie => favorityMovies.includes(movie.id.toString()))}
      onSelect={(movieId: string) => dispatch(removeMovie(movieId))}
      selectedMovies={favorityMovies}
    />
  ) : <></>
};

export default MyMoviesContainer;
