import { useEffect } from "react";
import MovieList from "../../components/MovieList"
import useDispatch from "../../hooks/useDispatch";
import useSelector from "../../hooks/useSelector";
import { addMovie, removeMovie } from "../../redux/action";
import { MovieRootState } from "../../redux/store";
import { fetchMovies } from "../../redux/thunk";

const AllMoviesContainer = () => {
  const dispatch = useDispatch();
  const { allMovies, loading, favorityMovies } = useSelector<MovieRootState>(state => ({
    favorityMovies: state.favorityMovies,
    allMovies: state.allMovies,
    loading: state.loading,
  }));

  useEffect(() => {
    dispatch(fetchMovies)
  }, [dispatch])


  return !loading ? (
    <MovieList
      title="All Movies"
      movies={allMovies}
      onSelect={(movieId: string) => {
        if (favorityMovies.includes(movieId)) dispatch(removeMovie(movieId))
        else {
          dispatch(addMovie(movieId))
        }
      }}
      selectedMovies={favorityMovies}
    />
  ) : <></>
};

export default AllMoviesContainer;
