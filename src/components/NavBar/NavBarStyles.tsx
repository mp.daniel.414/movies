import styled from "styled-components";

export const NavBarWrapper = styled.nav`
  width: 100%;
  height: 60px;
  display: flex;
  justify-content: center;
  background: #000;
`;

export const NavBarItem = styled.div`
  display: flex;
  padding: 16px;
  align-items: center;

  &:hover {
    transform: scale(1.04);
    transition: all 250ms ease-in-out;
  }

  & span {
    margin-right: 5px;
  }

  & svg {
    fill: #f9f9f9;
  }

  & a {
    display: flex;
    align-items: center;
    color: #f9f9f9;
    text-decoration: none;
  }
`;