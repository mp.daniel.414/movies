import React from "react";
import { Link } from "react-router-dom";
import MovieRoll from "../Icon/MovieRollIcon";
import StarIcon from "../Icon/StarIcon";
import { NavBarItem, NavBarWrapper } from "./NavBarStyles";

const NavBar = (): React.ReactElement => {
  return (
    <NavBarWrapper>
      <NavBarItem>
        <Link to="/">
          <span>
            <MovieRoll />
          </span>
          All Movies
        </Link>
      </NavBarItem>
      <NavBarItem>
        <Link to="/my-movies">
          <span>
            <StarIcon />
          </span>
          My Movies
        </Link>
      </NavBarItem>
    </NavBarWrapper>
  );
};

export default NavBar;