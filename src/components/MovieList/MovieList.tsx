import React from "react";
import { Movie } from "../../models/movie";
import MovieCard from "../MovieCard";
import { MovieListTitle, MovieListWrapper, MovieListCardsWrapper } from "./MovieListStyles";

interface MovieListProps {
  title: string;
  onSelect?: (movieId: string) => void | unknown;
  movies: Movie[];
  selectedMovies?: string[];
}

const MovieList = ({ title, movies, onSelect, selectedMovies = [] }: MovieListProps): React.ReactElement => {
  return (
    <MovieListWrapper>
      <MovieListTitle>{title}</MovieListTitle>
      <MovieListCardsWrapper>
        {movies.map((movie) => (
          <MovieCard key={movie.id} {...movie} onSelect={onSelect} selected={selectedMovies.includes(movie.id.toString())}/>
        ))}
      </MovieListCardsWrapper>
    </MovieListWrapper>
  )
  
}

export default MovieList;
