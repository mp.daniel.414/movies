import { ComponentProps } from "react";
import { Meta, Story } from "@storybook/react";
import MovieList from ".";

export default {
  title: "MovieList",
  Component: MovieList,
} as Meta;

const Template: Story<ComponentProps<typeof MovieList>> = (args: any) => <MovieList {...args} />;

export const Default = Template.bind({});

Default.args = {
  title: "List",
  movies: [
    {
      id: 123,
      title: "Shang-Chi and the Legend of the Ten Rings",
      posterPath: "https://image.tmdb.org/t/p/w500/cinER0ESG0eJ49kXlExM0MEWGxW.jpg",
      voteAverage: 8.3,
      releasedDate: new Date("2021-11-20"),
    },
    {
      id: 321,
      title: "Clifford the Big Red Dog",
      posterPath: "https://image.tmdb.org/t/p/w500/zBkHCpLmHjW2uVURs5uZkaVmgKR.jpg",
      voteAverage: 7.9,
      releasedDate: new Date(),
    },
    {
      id: 2,
      title: "Amina",
      posterPath: "https://image.tmdb.org/t/p/w500/4EJSMQOM1bZPHvzqAQe87suBxdf.jpg",
      voteAverage: 7.1,
      releasedDate: new Date(),
    },
    {
      id: 1,
      title: "Finch",
      posterPath: "https://image.tmdb.org/t/p/w500/oE6bhqqVFyIECtBzqIuvh6JdaB5.jpg",
      voteAverage: 8.2,
      releasedDate: new Date(),
    },
    {
      id: 5,
      title: "After We Fell",
      posterPath: "https://image.tmdb.org/t/p/w500/4vCh8R4yd6ybOmbxRAPOzaXmLTV.jpg",
      voteAverage: 6.5,
      releasedDate: new Date("2021-12-15"),
    }
  ]
};
