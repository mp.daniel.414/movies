import styled from "styled-components";

export const MovieListWrapper = styled.div`
  padding: 30px
`;

export const MovieListCardsWrapper = styled.div`
  display: grid;
  column-gap: 15px;
  row-gap: 30px;
  grid-template-columns: repeat(auto-fit, minmax(250px, 1fr));
  justify-items: center;
`;

export const MovieListTitle = styled.h3`
  font-size: 20px;
  color: #f9f9f9;
  line-height: 28px;
  font-weight: 600;
`;