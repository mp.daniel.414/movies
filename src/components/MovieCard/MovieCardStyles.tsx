import styled from "styled-components";

export const MovieCardTitle = styled.span`
  width: 80%;
  visibility: hidden;
  font-size: 18px;
  color: white;
  position: absolute;
  top: 12px;
  right: 0;
  left: 21px;
  text-overflow: ellipsis;
  overflow: hidden;
  white-space: nowrap;
`;

export const MovieCardReleasedDate = styled.span`
  position: absolute;
  visibility: hidden;
  bottom: 12px;
  right: 0;
  left: 21px;
  color: white;
  font-size: 18px;
`;

export const MovieCardVoteAverage = styled.span`
  position: absolute;
  visibility: hidden;
  bottom: 12px;
  right: 21px;
  color: white;
  font-size: 18px;
`;


export const MovieCardPoster = styled.img`
  display: block;
  max-width: 100%;
  width: 100%;
  border-radius: 4px;
  vertical-align: middle;
`;

export const MovieCardSelected = styled.div`
  position: absolute;
  width: 32px;
  height: 32px;
  top: 12px;
  right: 5px;
  display: flex;
  align-items: center;
  background: rgba(0,0,0,0.7);
  justify-content: center;
  border-radius: 50%;
  fill: #fff;
`;

export const MovieCardWrapper = styled.div`
  cursor: pointer;
  max-width: 340px;
  border-radius: 4px;
  box-shadow: rgba(0,0,0,0.69) 0px 26px 30px -10px,rgba(0,0,0,0.73) 0px 16px 10px -10px;
  transform: scale(1);
  transition: all 250ms ease-in-out;
  position: relative;
  box-sizing: border-box;

  &:hover {
    transform: scale(1.04);
    transition: all 250ms ease-in-out;
    box-shadow: rgba(0,0,0,0.8) 0px 40px 58px -16px,rgba(0,0,0,0.72) 0px 30px 22px -10px;
  }

  &:hover ${MovieCardPoster} {
    filter: brightness(0.5);
  }

  &:hover ${MovieCardTitle},
  &:hover ${MovieCardReleasedDate},
  &:hover ${MovieCardVoteAverage} {
    visibility: visible;
  }
`;

