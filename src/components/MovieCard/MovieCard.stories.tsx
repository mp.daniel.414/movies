import { ComponentProps } from "react";
import { Meta, Story } from "@storybook/react";
import MovieCard from ".";

export default {
  title: "MovieCard",
  Component: MovieCard,
} as Meta;

const Template: Story<ComponentProps<typeof MovieCard>> = (args: any) => <MovieCard {...args} />;

export const Default = Template.bind({});

Default.args = {
  title: "Shang-Chi and the Legend of the Ten Rings",
  posterPath: "https://image.tmdb.org/t/p/w500//cinER0ESG0eJ49kXlExM0MEWGxW.jpg",
  voteAverage: 7.9,
  releasedDate: new Date(),
};


export const EmptyPoster = Template.bind({});

EmptyPoster.args = {
  title: "Empty Poster Example",
  voteAverage: 5.2,
  releasedDate: new Date(),
};

export const StaredCard = Template.bind({});

StaredCard.args = {
  title: "Shang-Chi and the Legend of the Ten Rings",
  posterPath: "https://image.tmdb.org/t/p/w500//cinER0ESG0eJ49kXlExM0MEWGxW.jpg",
  voteAverage: 7.9,
  releasedDate: new Date(),
  selected: true,
};