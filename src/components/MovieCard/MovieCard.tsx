import React from "react";
import { Movie } from "../../models/movie";
import { MovieCardReleasedDate, MovieCardPoster, MovieCardTitle, MovieCardWrapper, MovieCardVoteAverage, MovieCardSelected } from "./MovieCardStyles";
import FullHeartIcon from "../Icon/FullHeartIcon";
import HeartIcon from "../Icon/HeartIcon";

interface MovieCardProps extends Movie {
  selected?: boolean
  onSelect?: (movieId: string) => void | unknown;
}

const MovieCard = ({ id, title, releasedDate, posterPath, voteAverage, selected, onSelect = () => null }: MovieCardProps): React.ReactElement<Movie> => {
return (
  <MovieCardWrapper onClick={() => onSelect(id.toString())}>
    <MovieCardPoster src={posterPath || "/images/empty-poster.png"} />
    <MovieCardSelected data-testid={selected ? "liked-icon" : "unliked-icon"}>
      {selected ?
      (
        <FullHeartIcon />
      ): (
        <HeartIcon />
      )}
    </MovieCardSelected>
    <MovieCardVoteAverage>{voteAverage}</MovieCardVoteAverage>
    <MovieCardTitle>{title}</MovieCardTitle>
    <MovieCardReleasedDate>{new Date(releasedDate).toLocaleDateString()}</MovieCardReleasedDate>
  </MovieCardWrapper>
 );
}

export default MovieCard;
