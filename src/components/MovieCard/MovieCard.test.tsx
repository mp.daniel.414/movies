import React from 'react';
import { render, screen } from '@testing-library/react';
import MovieCard from './MovieCard';

describe("MonisCard", () => {
  test("Should render properties properly", () => {
    const now = new Date();
    render(
      <MovieCard
        id={10}
        title= "Shang-Chi and the Legend of the Ten Rings"
        posterPath= "https://image.tmdb.org/t/p/w500//cinER0ESG0eJ49kXlExM0MEWGxW.jpg"
        voteAverage={7.9}
        releasedDate={now}
        selected
      />
    );
    const titleElement = screen.getByText(/Shang/i);
    expect(titleElement).toBeInTheDocument();
    const voteAverageElement = screen.getByText(/7.9/i);
    expect(voteAverageElement).toBeInTheDocument();
    const releasedDateElement = screen.getByText(now.toLocaleDateString());
    expect(releasedDateElement).toBeInTheDocument();
  });

  test("Should render the 'selected icon' when selected", () => {
    const now = new Date();
    render(
      <MovieCard
        id={10}
        title= "Shang-Chi and the Legend of the Ten Rings"
        posterPath= "https://image.tmdb.org/t/p/w500//cinER0ESG0eJ49kXlExM0MEWGxW.jpg"
        voteAverage={7.9}
        releasedDate={now}
        selected
      />
    );
    const icon = screen.getByTestId("liked-icon");
    expect(icon).toBeInTheDocument();
  });


  test("Should render the 'unselect icon' when not selected", () => {
    const now = new Date();
    render(
      <MovieCard
        id={10}
        title= "Shang-Chi and the Legend of the Ten Rings"
        posterPath= "https://image.tmdb.org/t/p/w500//cinER0ESG0eJ49kXlExM0MEWGxW.jpg"
        voteAverage={7.9}
        releasedDate={now}
      />
    );
    const icon = screen.getByTestId("unliked-icon");
    expect(icon).toBeInTheDocument();
  });
});
