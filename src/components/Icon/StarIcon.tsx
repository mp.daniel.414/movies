import React from "react"

const StarIcon = () => {
  return (
    <svg
      aria-hidden="true"
      aria-label="originals"
      color="#f9f9f9"
      viewBox="0 0 36 36"
      xmlns="http://www.w3.org/2000/svg"
      data-route="ORIGINAIS"
      className="prefix__sc-gzVnrw prefix__bmFKhl"
      style={{
        height: 24,
        minWidth: 24,
        width: 24,
        zIndex: "auto",
      }}
    >
      <path
        d="M17.625 26.028L10.44 30l1.373-8.412L6 15.631l8.033-1.228 3.592-7.653 3.592 7.653 8.033 1.228-5.813 5.957L24.81 30z"
        className="prefix__sc-bZQynM prefix__keKOpO"
      />
    </svg>
  )
}

export default StarIcon;
