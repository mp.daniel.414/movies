import React from 'react';
import { Routes, Route } from "react-router-dom";
import NavBar from './components/NavBar';
import HomePage from './pages/HomePage';
import MyMoviesPage from './pages/MyMoviesPage';

function App() {
  return (
    <div className="App" style={{ background: "#1a1d29" }}>
      <NavBar />
      <Routes>
        <Route path="/" element={<HomePage />} />
        <Route path="/my-movies" element={<MyMoviesPage />} />
      </Routes>  
    </div>
  );
}

export default App;
