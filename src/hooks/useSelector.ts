import { TypedUseSelectorHook, useSelector as reduxUseSelect } from "react-redux";
import { MovieRootState } from "../redux/store";

const useSelector: TypedUseSelectorHook<MovieRootState> = reduxUseSelect;

export default useSelector;
