import { useDispatch as reduxUseDispatch } from "react-redux";
import { MovieDispatch } from "../redux/store";

const useDispatch = () => reduxUseDispatch<MovieDispatch>();

export default useDispatch;
