export const apiUrl = "https://api.themoviedb.org/";
export const apiKey = process.env.REACT_APP_API_KEY; // Should be handle by .env as a secret
export const imageUrl = "https://image.tmdb.org/t/p/w500/";
