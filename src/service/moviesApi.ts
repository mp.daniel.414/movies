import { apiKey, apiUrl, imageUrl } from "../config/tmdb";
import { Movie } from "../models/movie";

export async function listMovies(): Promise<Movie[]> {
  const endpoint = "3/discover/movie";
  try {
    const response = await fetch(`${apiUrl}${endpoint}?api_key=${apiKey}&sort_by=popularity.desc&language=en-US`);
    const { results } = await response.json();
    return results.map((result: Record<string, any>): Movie => ({
      id: result.id,
      title: result.title,
      voteAverage: Number(result.vote_average),
      posterPath: `${imageUrl}${result.backdrop_path}`,
      releasedDate: new Date(result.release_date),
    }));
  } catch (error) {
    console.error(error);
    return [];
  }
}
