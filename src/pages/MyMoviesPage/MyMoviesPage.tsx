import MyMoviesContainer from "../../containers/MyMoviesContainer";

const MyMoviesPage = () => {
  return (
    <MyMoviesContainer />
  );
}

export default MyMoviesPage;
