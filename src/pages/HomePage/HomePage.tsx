import React from "react";
import AllMoviesContainer from "../../containers/AllMoviesContainer";

const HomePage = () => {
  return <AllMoviesContainer />;
}

export default HomePage;
