export class Movie {
  id: number;
  title: string;
  posterPath?: string;
  voteAverage: number;
  releasedDate: Date;
}