import { AnyAction } from "redux";
import { Movie } from "../models/movie";
import { ADD_MOVIE, LOADING_MOVIES, LOAD_MOVIES, REMOVE_MOVIE } from "./action";

export type ReducerState = {
  favorityMovies: string[];
  allMovies: Movie[];
  loading: boolean;
}

const inititalState = {
  favorityMovies: [],
  allMovies: [],
  loading: false,
};

export default function movieReducer(state: ReducerState = inititalState, action: AnyAction) {
  switch(action.type) {
    case ADD_MOVIE: {
      const { movieId } = action;
      const favorityMovies = ([] as string[]).concat(state.favorityMovies);
      return {
        ...state,
        favorityMovies: favorityMovies.concat(movieId)
      }
    }
    case REMOVE_MOVIE: {
      const { movieId } = action; 
      return {
        ...state,
        favorityMovies: state.favorityMovies.filter((movie) => movie !== movieId)
      };
    }
    case LOADING_MOVIES: {
      const { loading } = action;
      return {
        ...state,
        loading,
      };
    }
    case LOAD_MOVIES: {
      const { movies } = action;
      return {
        ...state,
        allMovies: movies,
        loading: false,
      };
    }
    default:
      return state;
  }
}