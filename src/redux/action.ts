import { Movie } from "../models/movie";

export const ADD_MOVIE = "ADD_MOVIE";
export const REMOVE_MOVIE = "REMOVE_MOVIE";
export const LOADING_MOVIES = "LOADING_MOVIES";
export const LOAD_MOVIES = "LOAD_MOVIES";

export function addMovie(movieId: string) {
  return {
    type: ADD_MOVIE,
    movieId,
  };
}

export function removeMovie(movieId: string) {
  return {
    type: REMOVE_MOVIE,
    movieId,
  };
}

export function loadingMovies(loading: boolean) {
  return {
    type: LOADING_MOVIES,
    loading,
  };
}

export function loadMovies(movies: Movie[]) {
  return {
    type: LOAD_MOVIES,
    movies: movies.map(movie => ({ ...movie, releasedDate: movie.releasedDate.toString() })),
  };
}
