import { listMovies } from "../service/moviesApi";
import { loadMovies } from "./action";

export async function fetchMovies(dispatch: any, getState: any) {
  const movies = await listMovies();

  dispatch(loadMovies(movies));
}
