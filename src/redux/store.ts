import { configureStore } from "@reduxjs/toolkit";
import movieReducer from "./reducer";

const store = configureStore({
  reducer: movieReducer,
});


export type MovieRootState = ReturnType<typeof store.getState>;
export type MovieDispatch = typeof store.dispatch;

export default store;
