# Movies App

This project was bootstrapped with [Create React App](https://github.com/facebook/create-react-app).

## Getting Started

### Project Structure
```
.
├── src                         # Source files - All project non-config files
    ├── actions
    ├── actions
    ├── components              # Unit property-controlled React components
    └── containers              # Macro components with business logic

src
└── components
    ├── Component.stories.tsx   # Component storybook file
    ├── ComponentStyles.tsx     # Component styles
    ├── Component.tsx           # Component source code
    └── index.ts                # Component export file
```

### Setup

Run ```yarn``` to install all dependencies

### Components

This project has ```Storybook``` setup, but no locally installed. To open it, you may need to install storybook globally.

Run to open:
```
yarn storybook
```


### Start project
Install dependencies:
```bash
yarn
```

Run project, **replace {REPLACE_TOKEN} with a valid TMDB API token**:
```bash
REACT_APP_API_KEY={REPLACE_TOKEN} yarn start
```


### Testing project
Run:
```bash
yarn test
```


## References
- [React Redux](https://react-redux.js.org/)
- [Chakra UI](https://chakra-ui.com/)